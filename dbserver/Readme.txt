# README #

Este � o sistema de votos de restaurantes desenvolvido por Vinicius Pires.

### Como Efetuar Login ###

Existem 5 Usuarios Mockados para entrar no sistema e votar

Vinicius
Joao
Pedro
Maria
Tereza

A senha de TODOS � 123

### Como Votar ###

 * Existem 7 Restaurantes mockados no sistema
 * Dois deles "Tio Sam Burguers" e "Dom Giovanni Ristorant" foram mockados como ganhadores da semana
 * Portanto n�o aparecer�o dispon�veis para a vota��o
 
*** A vota��o estar� encerrada ap�s o meio-dia, n�o sendo possivel votar
*** O sistema redireciona voce diretamente para o ranking do dia
*** O sistema automaticamente adicionar� o restaurante ganhador como um dos ganhadores no banco de dados fake

* Ap�s o voto o usuario � redirecionado para o ranking, so podendo votar novamente no dia seguinte 

### Como foi o desenvolvimento ###

*  O projeto roda necessariamente em um Apache Tomcat 8 e Java 8
*  Foi Desenvolvido com JSF 2 e Primefaces, pela facilidade e agilidade que os dois em conjunto trazem
*  Foi dividido em 4 Camadas (View, Controller, Service, DAO)
*  Regras de neg�cio importantes est�o apenas na service
*  Os m�todos importantes de neg�cio possuem testes unitarios
*  Devido ao mockito ter dados erros bizarros e eu perder muito tempo com isso, 
*  alguns metodos da controller que necessitavam de mocks e testes mais complexos nao foram feitos 

### Melhorias ###

 * Implementar persistencia em banco de dados
 * Implementar filtro/interceptor de seguran�a para as requisi��es
 * Implementar algum framework para inje��o de dependencias e Ioc para tirar os "new" do codigo e reduzir o acoplamento
 * Formatar a data da tela de vota��o
 * Implementar o CRUD de restaurantes e usuarios
 
 