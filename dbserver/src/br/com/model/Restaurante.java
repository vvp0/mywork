package br.com.model;

/**
 * 
 * @author Vinicius
 *
 */
public class Restaurante {

	private String nome;
	private String endereco;
	private Double precoKg;

	public Restaurante(String nome, String endereco, Double precoKg) {
		this.nome = nome;
		this.endereco = endereco;
		this.precoKg = precoKg;
	}

	public String getNome() {
		return nome;
	}

	public void setNome(String nome) {
		this.nome = nome;
	}

	public String getEndereco() {
		return endereco;
	}

	public void setEndereco(String endereco) {
		this.endereco = endereco;
	}

	public Double getPrecoKg() {
		return precoKg;
	}

	public void setPrecoKg(Double precoKg) {
		this.precoKg = precoKg;
	}

	@Override
	public boolean equals(Object obj) {

		Restaurante aux = (Restaurante) obj;

		if (this.nome.equals(aux.getEndereco()) && this.endereco.equals(aux.getEndereco())
				&& this.precoKg.equals(aux.getPrecoKg())) {
			return true;
		} else {
			return false;
		}

	}

}
