package br.com.model;

import java.time.LocalDate;

/**
 * 
 * @author Vinicius
 *
 */
public class Voto {
	
	private LocalDate data;
	private String usuario;
	private String restaurante;
	
	public Voto(LocalDate data, String usuario, String restaurante) {
		this.data = data;
		this.usuario = usuario;
		this.restaurante = restaurante;
	}
	
	public String getUsuario() {
		return usuario;
	}
	public void setUsuario(String usuario) {
		this.usuario = usuario;
	}
	public String getRestaurante() {
		return restaurante;
	}
	public void setRestaurante(String restaurante) {
		this.restaurante = restaurante;
	}
	
	public LocalDate getData() {
		return data;
	}
	public void setData(LocalDate data) {
		this.data = data;
	}
	
	@Override
	public boolean equals(Object obj) {
		Voto votoAux = (Voto) obj;
		
		if(this.data.equals(votoAux.getData()) && 
				this.restaurante.equals(votoAux.getRestaurante()) && 
				this.usuario.equals(votoAux.getUsuario())) {
			return true;
		}
		return false;
	}
	
	@Override
	public String toString() {
		return "Data: " + this.data + " Restaurante: " + this.restaurante + " Usuario: " + this.usuario;
	}
	
	@Override
	public int hashCode() {
		// TODO Auto-generated method stub
		return super.hashCode();
	}
	
	

}
