package br.com.model;

public class Ranking implements Comparable<Ranking> {

	private String nomeRestaurante;
	private int quantidadeVotos;
	
	public Ranking(String restaurante,int quantidadeVotos) {
		this.nomeRestaurante = restaurante;
		this.quantidadeVotos = quantidadeVotos;
	}
	
	public String getNomeRestaurante() {
		return nomeRestaurante;
	}
	public void setNomeRestaurante(String nomeRestaurante) {
		this.nomeRestaurante = nomeRestaurante;
	}
	public int getQuantidadeVotos() {
		return quantidadeVotos;
	}
	public void setQuantidadeVotos(int quantidadeVotos) {
		this.quantidadeVotos = quantidadeVotos;
	}

	@Override
	public int compareTo(Ranking o) {
		return o.quantidadeVotos - this.quantidadeVotos;
	}
	
}
