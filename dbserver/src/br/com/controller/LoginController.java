package br.com.controller;

import javax.annotation.PostConstruct;
import javax.faces.application.FacesMessage;
import javax.faces.bean.ManagedBean;
import javax.faces.context.ExternalContext;
import javax.faces.context.FacesContext;
import javax.servlet.http.HttpSession;

import br.com.dao.UsuarioDao;
import br.com.model.Usuario;
import br.com.service.UsuarioService;

@ManagedBean
public class LoginController {

	private String nome;
	private String senha;
	private UsuarioService usuarioService;
	

	@PostConstruct
	public void init() {
		usuarioService = new UsuarioService(new UsuarioDao());		
		new UsuarioDao().adiciona(new Usuario("Vinicius", "123"));
		new UsuarioDao().adiciona(new Usuario("Joao", "123"));
		new UsuarioDao().adiciona(new Usuario("Pedro", "123"));
		new UsuarioDao().adiciona(new Usuario("Maria", "123"));
		new UsuarioDao().adiciona(new Usuario("Tereza", "123"));
	}
	
	/**
	 * Verifica as credenciais e efetua o login
	 * 
	 * @return
	 */
	public String login() {
		boolean resultado  = this.usuarioService.verificaUsuario(new Usuario(nome, senha));
		
		if(resultado == true) {
			
			FacesContext fc = FacesContext.getCurrentInstance();
			ExternalContext ec = fc.getExternalContext();
			
			HttpSession session = (HttpSession) ec.getSession(true);
			session.setAttribute("usuario", new Usuario(nome, senha));	
			
			return "/index";
		} else {
			addMessage("Aten��o", "Usu�rio ou Senha Incorretos!");
			return "/login.xhtml";
		}
	}

	//TODO: Criar variavel para facilitar testes
	public void addMessage(String titulo, String detalhes) {
        FacesMessage message = new FacesMessage(FacesMessage.SEVERITY_WARN, titulo, detalhes);
        FacesContext.getCurrentInstance().addMessage(null, message);
    }
	
	public UsuarioService getUsuarioService() {
		return usuarioService;
	}

	public void setUsuarioService(UsuarioService usuarioService) {
		this.usuarioService = usuarioService;
	}

	public String getNome() {
		return nome;
	}

	public void setNome(String nome) {
		this.nome = nome;
	}

	public String getSenha() {
		return senha;
	}

	public void setSenha(String senha) {
		this.senha = senha;
	}
}
