package br.com.controller;

import java.time.LocalDate;
import java.util.Collections;
import java.util.List;

import javax.annotation.PostConstruct;
import javax.faces.application.FacesMessage;
import javax.faces.bean.ManagedBean;
import javax.faces.context.ExternalContext;
import javax.faces.context.FacesContext;
import javax.servlet.http.HttpSession;

import br.com.dao.RestauranteDao;
import br.com.dao.VotacaoDao;
import br.com.model.Ranking;
import br.com.model.Restaurante;
import br.com.model.Usuario;
import br.com.model.Voto;
import br.com.service.RestauranteService;
import br.com.service.VotacaoService;

@ManagedBean
public class VotacaoController {

	private List<Restaurante> listRestaurantes;
	private RestauranteService restauranteService;
	private VotacaoService votacaoService;
	private List<Ranking> ranking;
	private LocalDate data;

	@PostConstruct
	public void init() {
		restauranteService = new RestauranteService(new RestauranteDao());
		listRestaurantes = restauranteService.getListRestaurante();
		votacaoService = new VotacaoService(new VotacaoDao());
		data = LocalDate.now();
	}

	/**
	 * 
	 * @param restaurante
	 * @return
	 */
	public String votar(String restaurante) {

		
		try {
			
			FacesContext fc = FacesContext.getCurrentInstance();
			ExternalContext ec = fc.getExternalContext();
			HttpSession session = (HttpSession) ec.getSession(false);

			Voto voto = new Voto(LocalDate.now(), ((Usuario) session.getAttribute("usuario")).getNome(), restaurante);

			boolean votacaoAberta = validaVotacao();
			
			if(votacaoAberta == false) {
				
				votacaoService.adicionaGanhador(LocalDate.now(), getRanking().get(0).getNomeRestaurante(), listRestaurantes);
				return "/ranking";
			}
			
			boolean resultado = validaVoto(voto);

			if (resultado == true) {
				votacaoService.votar(voto);
				addMessageSucess("Sucesso!", "Voto Realizado!");
				return "/ranking";
			} else {
				return "/ranking";
			}
			
		} catch (Exception e) {
			addMessageError("Aten��o", "Ocorreu um erro no Sistema!");
			return "/login";
		}
		

	}

	/**
	 * 
	 * @param voto
	 * @return
	 */
	public boolean validaVoto(Voto voto) {

		try {
			votacaoService.validaVoto(voto);
		} catch (Exception e) {
			addMessageError("Aten��o", e.getMessage());
			return false;
		}

		return true;
	}
	
	/**
	 * 
	 */
	public boolean validaVotacao() {
		
		try {
			votacaoService.validaVotacao();
			return true;
		} catch (Exception e) {
			addMessageError("Aten��o", e.getMessage());
			return false;
		}
		
	}

	
	public void addMessageWarn(String titulo, String detalhes) {
		FacesMessage message = new FacesMessage(FacesMessage.SEVERITY_WARN, titulo, detalhes);
		FacesContext.getCurrentInstance().addMessage(null, message);
	}

	
	public void addMessageError(String titulo, String detalhes) {
		FacesMessage message = new FacesMessage(FacesMessage.SEVERITY_ERROR, titulo, detalhes);
		FacesContext.getCurrentInstance().addMessage(null, message);
	}

	
	public void addMessageSucess(String titulo, String detalhes) {
		FacesMessage message = new FacesMessage(FacesMessage.SEVERITY_INFO, titulo, detalhes);
		FacesContext.getCurrentInstance().addMessage(null, message);
	}

	public List<Restaurante> getListRestaurantes() {
		return listRestaurantes;
	}

	public void setListRestaurantes(List<Restaurante> listRestaurates) {
		this.listRestaurantes = listRestaurates;
	}

	public RestauranteService getRestauranteService() {
		return restauranteService;
	}

	public void setRestauranteService(RestauranteService restauranteService) {
		this.restauranteService = restauranteService;
	}

	public VotacaoService getVotacaoService() {
		return votacaoService;
	}

	public void setVotacaoService(VotacaoService votacaoService) {
		this.votacaoService = votacaoService;
	}

	public List<Ranking> getRanking() {
		
		List<Ranking> listRanking = votacaoService.getGanhadores(listRestaurantes);
		
		Collections.sort(listRanking);
		
		return listRanking;
	}

	public void setRanking(List<Ranking> ranking) {
		this.ranking = ranking;
	}

	public LocalDate getData() {
		return data;
	}

	public void setData(LocalDate data) {
		this.data = data;
	}


}
