package br.com.dao;

import java.util.ArrayList;
import java.util.List;

import br.com.model.Restaurante;

/**
 * 
 * @author Vinicius
 *
 */
public class RestauranteDao {

	private List<Restaurante> listRestaurante = new ArrayList<>();

	public RestauranteDao() {
		
		Restaurante restaurante1 = new Restaurante("Tia Chica Foods", "Av. Ipiranga 505", 30.50);
		Restaurante restaurante2 = new Restaurante("Madero Hamburgueria", "Av. Ipiranga 1024", 40.50);
		Restaurante restaurante3 = new Restaurante("Outback Steakhouse", "Av. Assis Brasil 247", 50.50);
		Restaurante restaurante4 = new Restaurante("Churrascaria dos Pampas", "Rua Comendador Roseira 85", 24.90);
		Restaurante restaurante5 = new Restaurante("Tio Sam Burguers", "Av. Getulio Vargas 202", 14.50);
		Restaurante restaurante6 = new Restaurante("Dom Giovanni Ristorant", "Av. Kennedy 2054", 23.90);
		Restaurante restaurante7 = new Restaurante("Hard Rock Caffe", "Rua. Hard Rock 001", 60.00);
		
		listRestaurante.add(restaurante1);
		listRestaurante.add(restaurante2);
		listRestaurante.add(restaurante3);
		listRestaurante.add(restaurante4);
		listRestaurante.add(restaurante5);
		listRestaurante.add(restaurante6);
		listRestaurante.add(restaurante7);
		
		
	}
	
	public List<Restaurante> getListRestaurante() {
		return listRestaurante;
	}

	public void setListRestaurante(List<Restaurante> listRestaurante) {
		this.listRestaurante = listRestaurante;
	} 
	
}
