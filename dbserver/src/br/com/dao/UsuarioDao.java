package br.com.dao;

import java.util.ArrayList;
import java.util.List;

import br.com.model.Usuario;

public class UsuarioDao {

	private static List<Usuario> listUsuarios = new ArrayList<>();

	public void adiciona(Usuario usuario) {
		this.listUsuarios.add(usuario);
	}
	
	public void deleta(Usuario usuario) {
		this.listUsuarios.remove(usuario);
	}
	
	public Usuario getUsuario(String nome) {
		for (Usuario usuario : listUsuarios) {
			
			if(usuario.getNome().equals(nome)) {
				return usuario;
			}
			
		}
		
		return null;
	}
	
	public static List<Usuario> getListUsuarios() {
		return listUsuarios;
	}


	public static void setListUsuarios(List<Usuario> listUsuarios) {
		UsuarioDao.listUsuarios = listUsuarios;
	}


	
		
	
	
}
