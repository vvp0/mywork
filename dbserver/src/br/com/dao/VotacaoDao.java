package br.com.dao;

import java.time.LocalDate;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import br.com.model.Restaurante;
import br.com.model.Voto;

/**
 * 
 * @author Vinicius
 *
 */
public class VotacaoDao {

	private static List<Voto> listVotacao = new ArrayList<>();
	private static Map<LocalDate, Restaurante> mapGanhador = new HashMap<>();
	private static List<String> ranking = new ArrayList<>();
	
	public VotacaoDao() {
	}
	
	public boolean votar(Voto voto) {
		ranking.add(voto.getRestaurante());
		return listVotacao.add(voto);
		
	}
	
	public void adicionaGanhador(LocalDate data, Restaurante restaurante) {
		this.mapGanhador.put(data, restaurante);
	}

	public static List<Voto> getListVotacao() {
		return listVotacao;
	}

	public static void setListVotacao(List<Voto> listVotacao) {
		VotacaoDao.listVotacao = listVotacao;
	}

	public static Map<LocalDate, Restaurante> getMapGanhador() {
		
		mapGanhador.put(LocalDate.now().minusDays(2), new Restaurante("Tio Sam Burguers", "Av. Getulio Vargas 202", 14.50));
		mapGanhador.put(LocalDate.now().minusDays(3), new Restaurante("Dom Giovanni Ristorant", "Av. Kennedy 2054", 23.90));
		
		return mapGanhador;
	}

	public static void setMapGanhador(Map<LocalDate, Restaurante> mapGanhador) {
		VotacaoDao.mapGanhador = mapGanhador;
	}

	/**
	 * 
	 * @param dia
	 * @return
	 */
	public List<Voto> getVotosDia(LocalDate dia) {
		
		List<Voto> listVotos = new ArrayList<>();
		
		for (Voto voto : listVotacao) {
			
			if(dia.equals(voto.getData())) {
				listVotos.add(voto);
			}
			
		}
		
		return listVotos;
	}

	public static List<String> getRanking() {
		return ranking;
	}

	public static void setRanking(List<String> ranking) {
		VotacaoDao.ranking = ranking;
	}
	
	
}
