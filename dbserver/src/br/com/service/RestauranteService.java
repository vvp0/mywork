package br.com.service;

import java.time.LocalDate;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import br.com.dao.RestauranteDao;
import br.com.dao.VotacaoDao;
import br.com.model.Restaurante;

/**
 * 
 * @author Vinicius
 *
 */
public class RestauranteService {

	private RestauranteDao restauranteDao;
	private VotacaoDao votacaoDao;
	
	
	public RestauranteService(RestauranteDao restauranteDao) {
		this.restauranteDao = restauranteDao;
		this.votacaoDao = new VotacaoDao();
	}
	
	public List<Restaurante> getListRestaurante() {
		return removeRestaurantesGanhadores(restauranteDao.getListRestaurante());
	}
	
	/**
	 * Remove restaurante que ja ganhou na semana
	 * @param listRestaurantes
	 * @return
	 */
	private List<Restaurante> removeRestaurantesGanhadores(List<Restaurante> listRestaurantes) {
		
		HashMap<LocalDate, Restaurante> ganhadores = (HashMap<LocalDate, Restaurante>) VotacaoDao.getMapGanhador();
		List<Integer> listRemover = new ArrayList<>();
		
		for (int aux = 0; aux < listRestaurantes.size(); aux++) {
			for (int i = 1; i < 8; i++) {
				
				if(ganhadores.containsKey(LocalDate.now().minusDays(i))) {
					
					Restaurante rest = ganhadores.get(LocalDate.now().minusDays(i));
					
					if(rest.getNome().equals(listRestaurantes.get(aux).getNome())) {
						listRemover.add(aux);
						listRestaurantes.remove(aux);
						aux--;
					}
					
				}
				
			}
		}				
				
		return listRestaurantes;

	}

	public RestauranteDao getRestauranteDao() {
		return restauranteDao;
	}

	public void setRestauranteDao(RestauranteDao restauranteDao) {
		this.restauranteDao = restauranteDao;
	}

	public VotacaoDao getVotacaoDao() {
		return votacaoDao;
	}

	public void setVotacaoDao(VotacaoDao votacaoDao) {
		this.votacaoDao = votacaoDao;
	}
	
}
