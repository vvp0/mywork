package br.com.service;

import br.com.dao.UsuarioDao;
import br.com.model.Usuario;

/**
 * 
 * @author Vinicius
 *
 */
public class UsuarioService {

	private UsuarioDao usuarioDao;

	/**
	 * 
	 * @param usuarioDao
	 */
	public UsuarioService(UsuarioDao usuarioDao) {
		this.usuarioDao = usuarioDao;
	}
	
	/**
	 * 
	 * @param usuario
	 */
	public void adiciona(Usuario usuario) {
		this.usuarioDao.adiciona(usuario);
	}
	
	/**
	 * 
	 * @param usuario
	 */
	public void deleta(Usuario usuario) {
		this.usuarioDao.deleta(usuario);
	}
	
	/**
	 * 
	 * @param nome
	 * @return Usuario encontrado
	 */
	public Usuario getUsuario(String nome) {
		return usuarioDao.getUsuario(nome);
	}

	public UsuarioDao getUsuarioDao() {
		return usuarioDao;
	}

	public void setUsuarioDao(UsuarioDao usuarioDao) {
		this.usuarioDao = usuarioDao;
	}

	/**
	 * Verifica se o usuario existe e a senha esta correta
	 * @param usuario
	 * @return
	 */
	public boolean verificaUsuario(Usuario usuario) {
		Usuario usuarioLogin = this.usuarioDao.getUsuario(usuario.getNome());
		if(usuarioLogin == null) {
			return false;
		} else if (usuario.getSenha().equals(usuarioLogin.getSenha())) {
			return true;
		} else {
			return false;
		}
	}

}
