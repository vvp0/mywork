package br.com.service;

import java.time.LocalDate;
import java.time.LocalTime;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import br.com.dao.VotacaoDao;
import br.com.model.Ranking;
import br.com.model.Restaurante;
import br.com.model.Voto;

/**
 * 
 * @author Vinicius
 *
 */
public class VotacaoService {
	
	private VotacaoDao votacaoDao;

	public VotacaoService(VotacaoDao votacaoDao) {
		this.votacaoDao = votacaoDao;
	}
	

	/**
	 * 
	 * @param voto
	 * @throws Exception
	 */
	public void validaVoto(Voto voto) throws Exception {
		
		validaVotoUsuarioNoDia(voto);
		
	}

	/**
	 * Verifica se o Usuario ja votou no dia 
	 * @param voto
	 * @throws Exception
	 */
	private void validaVotoUsuarioNoDia(Voto voto) throws Exception {
		List<Voto> listVotosDoDia = votacaoDao.getVotosDia(LocalDate.now());
		
		for (Voto votoList : listVotosDoDia) {
			
			if(voto.getData().equals(votoList.getData()) && voto.getUsuario().equals(votoList.getUsuario())) {
				throw new Exception("Usu�rio J� Votou Hoje");
			}
			
		}
	}
	
	/**
	 * 
	 * @param voto
	 */
	public void votar(Voto voto) {
		this.votacaoDao.votar(voto);
	}
	
	/**
	 * Adiciona Restaurante ganhador do dia
	 * @param data
	 * @param restaurante
	 * @param listRestaurantes
	 */
	public void adicionaGanhador(LocalDate data, String restaurante, List<Restaurante> listRestaurantes) {
		
		for (Restaurante restaurante2 : listRestaurantes) {
			
			if(restaurante2.getNome().equals(restaurante)) {
				
				if(votacaoDao.getMapGanhador().containsKey(data) == false) {
					this.votacaoDao.adicionaGanhador(data, restaurante2);
					break;
				}	
				
			}
			
		}
		
		
	}

	/**
	 * Verifica se a vota��o esta encerrada
	 * @throws Exception
	 */
	public void validaVotacao() throws Exception {
		
		if(LocalTime.now().getHour() >=  LocalTime.NOON.getHour()) {
			throw new Exception("Vota��o Encerrada!");
		}
		
	}
	
	/**
	 * Retorna a lista de restaurantes ganhadores
	 * @param listRestaurantes
	 */
	public List<Ranking> getGanhadores(List<Restaurante> listRestaurantes) {
		
		List<String> listVotos = votacaoDao.getRanking();
		List<Ranking> ranking = new ArrayList<>();
		
		
		for (Restaurante restaurante : listRestaurantes) {
			int numeroVotos = Collections.frequency(listVotos, restaurante.getNome());
			ranking.add(new Ranking(restaurante.getNome(),numeroVotos));
		}
		
		return ranking;
	}
	
	public VotacaoDao getVotacaoDao() {
		return votacaoDao;
	}

	public void setVotacaoDao(VotacaoDao votacaoDao) {
		this.votacaoDao = votacaoDao;
	}

}
