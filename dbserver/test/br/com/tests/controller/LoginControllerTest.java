package br.com.tests.controller;

import static org.junit.Assert.assertEquals;

import org.junit.Before;
import org.junit.Test;

import br.com.controller.LoginController;

public class LoginControllerTest {

	private LoginController loginController;
	
	@Before
	public void init(){
		loginController = new LoginController();	        
	}
	
	@Test
	public void deveRetornarLoginErrado() {
		
		loginController.setNome("Jo�o");
		loginController.setSenha("123");
		
		assertEquals("/login", loginController.login());
		
	}
	
	@Test
	public void deveRetornarIndex() {
		
		loginController.setNome("Vinicius");
		loginController.setSenha("123");
		
		assertEquals("/index", loginController.login());
		
	}
	
}
