package br.com.tests.service;

import static org.junit.Assert.assertEquals;

import org.junit.Before;
import org.junit.Test;

import br.com.dao.UsuarioDao;
import br.com.model.Usuario;
import br.com.service.UsuarioService;

public class UsuarioServiceTest {

	private Usuario usuario;
	private UsuarioService usuarioService;
	
	 @Before
	  public void init(){
		usuario = new Usuario("Vinicius","123"); 	    
	    usuarioService = new UsuarioService(new UsuarioDao());
	  }
	
	@Test
	public void deveAdicionarUsuario() {
		
		usuarioService.adiciona(usuario);
		
		assertEquals(usuario, usuarioService.getUsuario(usuario.getNome()));
		
	}
	
	@Test
	public void deveDeletarUsuario() {
		
		usuarioService.adiciona(usuario);
		
		usuarioService.deleta(usuario);
		
		assertEquals(null, usuarioService.getUsuario(usuario.getNome()));
		
	}
	
	@Test
	public void deveTrazerUsuario() {
		
		usuarioService.adiciona(usuario);
		
		assertEquals(usuario,usuarioService.getUsuario(usuario.getNome()));
	}
	
	@Test
	public void verificaSenhaNomeUsuario() {
		usuarioService.adiciona(usuario);		
		assertEquals(false, usuarioService.verificaUsuario(new Usuario("Pedro", "123")));
		assertEquals(false, usuarioService.verificaUsuario(new Usuario("Vinicius", "1234")));
		assertEquals(true, usuarioService.verificaUsuario(new Usuario("Vinicius", "123")));
	}
	
}
