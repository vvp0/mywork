package br.com.tests.service;

import static org.junit.Assert.*;

import java.time.LocalDate;

import org.junit.Before;
import org.junit.Test;

import br.com.dao.RestauranteDao;
import br.com.dao.VotacaoDao;
import br.com.model.Restaurante;
import br.com.service.RestauranteService;

public class RestauranteServiceTest {

	
	private RestauranteService restauranteService;
	private VotacaoDao votacaoDao;
	
	@Before
	public void init() {
		restauranteService = new RestauranteService(new RestauranteDao());
		votacaoDao = new VotacaoDao();
	}
	
	@Test
	public void deveRemoverGanhadores() {
		
		votacaoDao.adicionaGanhador(LocalDate.now().minusDays(5), new Restaurante("Hard Rock Caffe", "Rua. Hard Rock 001", 60.00));
		
		assertEquals(4, restauranteService.getListRestaurante().size());
	}
	
}
