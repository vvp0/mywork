package br.com.tests.service;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.fail;

import java.time.LocalDate;
import java.util.ArrayList;

import org.junit.Before;
import org.junit.Test;

import br.com.dao.VotacaoDao;
import br.com.model.Voto;
import br.com.service.VotacaoService;

public class VotacaoServiceTest {

	private VotacaoDao votacaoDao;
	private VotacaoService votacaoService;
	
	@Before
	public void init() {
		votacaoDao = new VotacaoDao();
		votacaoService = new VotacaoService(votacaoDao);
	}
	
	@Test
	public void deveImpedirVotoMesmoDia() {
		
		Voto voto = new Voto(LocalDate.now(),"Vinicius" , "Madero Hamburgueria");
		votacaoService.votar(voto);
		
		try {
			votacaoService.validaVoto(voto);
			fail("Esperado uma Exception");
		} catch (Exception e) {
			assertEquals("Usu�rio J� Votou Hoje",e.getMessage());
		}
		
	}
	
	@Test
	public void deveComputarVoto() {
		votacaoDao.setListVotacao(new ArrayList<>());
		Voto voto = new Voto(LocalDate.now(),"Pedro" , "Madero Hamburgueria");
		votacaoService.votar(voto);
		
		assertEquals(voto, votacaoDao.getListVotacao().get(0));
		
		
	}
	
	
	
}
