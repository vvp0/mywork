package br.com.tests.dao;

import static org.junit.Assert.assertEquals;

import java.time.LocalDate;
import java.util.ArrayList;

import org.junit.Before;
import org.junit.Test;

import br.com.dao.VotacaoDao;
import br.com.model.Restaurante;
import br.com.model.Voto;

public class VotacaoDaoTest {

	private VotacaoDao votacaoDao;
	
	@Before
	public void init(){
		votacaoDao = new VotacaoDao();	        
	}
	
	@Test
	public void deveComputarVoto() {
		
		votacaoDao.setListVotacao(new ArrayList<>());
		votacaoDao.votar(new Voto(LocalDate.now(), "Vinicius", "Outback Steakhouse"));
		votacaoDao.votar(new Voto(LocalDate.now(), "Jo�o", "Madero Hamburgueria"));
		
		assertEquals(new Voto(LocalDate.now(), "Vinicius", "Outback Steakhouse"), votacaoDao.getListVotacao().get(0));
		assertEquals(new Voto(LocalDate.now(), "Jo�o", "Madero Hamburgueria"), votacaoDao.getListVotacao().get(1));
		
	}
	
	@Test
	public void deveAdicionarGanhador() {
		
		votacaoDao.adicionaGanhador(LocalDate.now(), new Restaurante("Madero Hamburgueria", "Av. Ipiranga 1024", 40.50));
		
		assertEquals("Madero Hamburgueria", votacaoDao.getMapGanhador().get(LocalDate.now()).getNome());
		
	}
	
	@Test
	public void deveRetornarVotosApenasDoDia() {
		
		votacaoDao.setListVotacao(new ArrayList<>());
		Voto voto1 = new Voto(LocalDate.now().minusDays(2), "Vinicius", "Outback Steakhouse");
		Voto voto2 = new Voto(LocalDate.now().minusDays(3), "Jo�o", "Madero Hamburgueria");
		
		votacaoDao.votar(voto1);
		votacaoDao.votar(voto2);
		
		Voto voto = votacaoDao.getVotosDia(LocalDate.now().minusDays(2)).get(0);
		
		assertEquals(voto1, voto);
		
	}
	
	
}
