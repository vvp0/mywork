package br.com.tests.dao;
import static org.junit.Assert.assertEquals;

import org.junit.Before;
import org.junit.Test;

import br.com.dao.UsuarioDao;
import br.com.model.Usuario;

public class UsuarioDaoTest {

	
	private Usuario usuario;
	private UsuarioDao usuarioDao;
	
	 @Before
	  public void init(){
		usuario = new Usuario("Vinicius","123"); 	    
	    usuarioDao = new UsuarioDao();
	  }
	
	@Test
	public void deveAdicionarUsuario() {
		
		usuarioDao.adiciona(usuario);
		
		assertEquals(usuario, usuarioDao.getUsuario(usuario.getNome()));
		
	}
	
	@Test
	public void deveDeletarUsuario() {
		
		usuarioDao.adiciona(usuario);
		
		usuarioDao.deleta(usuario);
		
		assertEquals(null, usuarioDao.getUsuario(usuario.getNome()));
		
	}
	
	@Test
	public void deveTrazerUsuario() {
		
		usuarioDao.adiciona(usuario);
		
		assertEquals(usuario,usuarioDao.getUsuario(usuario.getNome()));
	}
	
}
